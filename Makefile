INSTALL	:= install
DESTDIR	:=
PIC	:= -fPIC
LD_D	:= $(CC) -shared
CFLAGS	:=
SHARE	:= -g $(PIC)


pam_client: pam_client.c
	$(CC) $(CFLAGS) $(SHARE) -c pam_client.c -o pam_client.o
	$(LD_D)  -o pam_client.so pam_client.o  -lc -lpam -lldap

clean: pam_client.so pam_client.o
	$(RM) pam_client.o
	$(RM) pam_client.so
	
install: pam_client.so
	$(INSTALL) -D -m 0755 pam_client.so $(DESTDIR)/lib/security/pam_client.so

uninstall: $(DESTDIR)/lib/security/pam_client.so
	$(RM) $(DESTDIR)/lib/security/pam_client.so
